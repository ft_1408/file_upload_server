<?php

require_once "bootstrap/bootstrap.php";

use Symfony\Component\Process\Process;
gc_enable();

// Logging PID
if(!file_put_contents(__DIR__."/.pid", getmypid())){
    echo "Can't create a .pid file in " . __DIR__;
}

echo "Script started " . date("Y-m-d H:i:s");

$processPrinter = function ($type, $buffer){
    if (Process::ERR === $type) {
        echo "Worker crashed" . $buffer;
    } else {
        echo $buffer;
    }
};

// Настройки запускаемых воркеров
$workers_paths = array(
			array('path'=> __DIR__.'/worker/UploadWorker.php', 'count'=>2),
            array('path'=> __DIR__.'/worker/DeleteWorker.php', 'count'=>2));

// Mainloop
while(1){
    $workers = array();
    $start_time = microtime(1);
    $start_memory = memory_get_usage(true);
    echo "Loop started " . date("Y-m-d H:i:s");

    // Starting workers
    foreach ($workers_paths as $worker_settings) {

        // Если не указанно колличество процессов - сводим к еденице
        if(!isset($worker_settings['count'])){
            $worker_settings['count'] = 1;
        }

        // Получаем сколько раз нужно запустить конкретный процесс
        $count = $worker_settings['count'];
        // Путь к php - файлу
        $path  = $worker_settings['path'];

        // Цикл запуска процессов
        for($i = 0; $i <= $count; $i++){

            $worker = new Process("/usr/bin/php $path", null,null,null,null);
            $worker->start();

            // 10 Minutes timeout
            $worker->setTimeout(60 * 10);

            $workers[] = $worker;
        }
    }

    echo "Processing... " . date("Y-m-d H:i:s");

    // Waiting workers
    foreach ($workers as $worker) {
        if($worker->isRunning()){
            $worker->wait($processPrinter);
        }
    }

    // Freeing memory
	foreach ($workers as $worker) {
        unset($worker);
    }
    unset($workers);

	$exec_time = microtime(1) - $start_time;
    $datetime = new \DateTime();

    $end_memory = memory_get_usage(true);
    $diff_memory = round ( ( $end_memory - $start_memory ) / (1024*1024), 2);
    $used_memory = round( $end_memory / (1024*1024) , 2);
    $peak_memory = round( memory_get_peak_usage() / (1024*1024) , 2);

    echo "Loop ended " . $datetime->format("Y-m-d H:i:s");

    print_r(array(
        'exec_time' => $exec_time,
        'peak_memory_usage' => $peak_memory . " MB",
        'current_memory_usage' => $used_memory . " MB",
        'diff_memory_usage' => $diff_memory . " MB"
    ) );

    // Run script not more than once every approx. 15 seconds
    if($exec_time < 15){
        echo "Sleeping...";
        $sleep_time = round(15 - $exec_time);
        sleep($sleep_time);
    }

    gc_collect_cycles();
}

gc_disable();
echo "Script finished " . date("Y-m-d H:i:s");
