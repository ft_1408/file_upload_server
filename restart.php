<?php
$pidfile = __DIR__ . "/.pid";
$exec_file = __DIR__ . "/startup.php";
$log = "/dev/null";

$is_stopped = false;
$result = shell_exec( "kill `cat $pidfile`");
if(empty($result)){
    echo "Server stopped" . PHP_EOL;
    $is_stopped = true;
} else{
    echo "Server NOT stopped. Please kill the process manualy" . PHP_EOL;
    echo "PID file: $pidfile" . PHP_EOL;
}


if($is_stopped){
    $lines = 0;
    if(file_exists($pidfile)){
        $lines = shell_exec("ps -p `cat $pidfile` | wc -l");
    }

    $number = trim($lines);
    if(trim($lines) < 2){
        echo "Restarting server..." . PHP_EOL;

        $is_started = shell_exec( "php -f $exec_file > $log 2>&1 &" );
        if(empty($is_started)){
            echo "Server restarted." . PHP_EOL;
        }else{
            echo "Error. Server NOT started." . PHP_EOL;
        }
    } else {
        echo "Server is running" . PHP_EOL;
    }
}
