<?php
$pidfile = __DIR__ . "/.pid";

if(!shell_exec( "kill `cat $pidfile`" )){
    echo "Server stopped" . PHP_EOL;
} else{
    echo "Server NOT stopped. Please kill the process manualy" . PHP_EOL;
    echo "PID file: $pidfile" . PHP_EOL;
}
