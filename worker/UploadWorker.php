<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
use Worker\UploadWorker;

require_once __DIR__."/../bootstrap/bootstrap.php";
require_once __DIR__."/../src/workers/Worker.php";
require_once __DIR__."/../src/workers/UploadWorker.php";

try{

	$upload = new UploadWorker($app);
	$upload->start();

}catch(Exception $e){
	// $app['logger']->write("$e", "CRITICAL");
	print_r($e);
}
