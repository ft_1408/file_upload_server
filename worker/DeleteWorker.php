<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
use Worker\DeleteWorker;

require_once __DIR__."/../bootstrap/bootstrap.php";
require_once __DIR__."/../src/workers/Worker.php";
require_once __DIR__."/../src/workers/DeleteWorker.php";

try{

	$upload = new DeleteWorker($app);
	$upload->start();

}catch(Exception $e){
	// $app['logger']->write("$e", "CRITICAL");
	print_r($e);
}
