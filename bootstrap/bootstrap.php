<?php
use Monolog\Handler\StreamHandler;
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

require_once __DIR__."/../vendor/autoload.php";
require_once __DIR__."/../src/Log.php";


function __autoload($class_name) {
    include '../src/workers/'.$class_name . '.php';
}


// Директория с конфигами
$config_dir = __DIR__."/../config/";


$app = new Pimple();

// Парсим конфиги реббита
$app['amqp_conf'] = Spyc::YAMLLoad($config_dir.'rabbit.yml');


// Парсим конфиги mySql
$app['db_conf'] = Spyc::YAMLLoad($config_dir.'db.yml');

// Получаем соединение mySql
$app['dbh'] = function ($app) {

	$conf = $app['db_conf']['mySql'];
	$host = $conf['host'];
	$db   = $conf['db'];
	$u    = $conf['user'];
	$p    = $conf['password'];

	try{
		return new PDO("mysql:host=$host;dbname=$db", $u, $p);
	}catch(PDOException $e){
		print_r($e);
	}
};

$app['logger'] = function ($app) {
	return new Log($app);
};

$app['StreamHandler'] = function ($app) {
	$log = __DIR__."/../logs/";
	$date = date('d.m.Y', time());

	if(!file_exists($log . $date . '.txt')){
		file_put_contents($log . $date . '.txt', '');
	}

	return new StreamHandler($log . $date . '.txt');
};

$app['mail_config'] = Spyc::YAMLLoad($config_dir.'mail.yml');

$app['rabbit'] = function ($app) {
	try{
		$config = $app['amqp_conf'];
		// Создаем подключение к ребиту
		$connection = new AMQPConnection($config['sever']['address'],
										 $config['sever']['port'],
										 $config['sever']['user'],
										 $config['sever']['password'],
										 $config['sever']['vhost']);

		return $connection;

	}catch(Exception $e){
		$app['loger']->write($e->getMessage(), "CRITICAL");
	}
};

// Получаем конфиги mongo
$app['mdb_config'] =  Spyc::YAMLLoad($config_dir.'mdb.yml');

// Подключение кmongo
$app['mdb'] = function ($app) {
	$config = $app['mdb_config'];

	try{
		// Пытаемся подключиться mongodb
		$mdb = new MongoClient("mongodb://{$config['user']}:{$config['password']}@{$config['server']}:{$config['port']}/{$config['db']}");

		return $mdb;
	}catch(Exception $e){
		// Если подключиться не удалось отправлеям сообщение о критической ошибке
		$app['logger']->write($e->getMessage(), "CRITICAL");
	}
};

// Получаем основной конфиг
$app['main_config'] = Spyc::YAMLLoad($config_dir.'main.yml');
