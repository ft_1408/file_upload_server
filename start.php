<?php
$log = "/dev/null";

$pidfile = __DIR__ . "/.pid";
$exec_file = __DIR__ . "/startup.php";

$lines = 0;
if(file_exists($pidfile)){
    $lines = shell_exec("ps -p `cat $pidfile` | wc -l");
}

$number = trim($lines);
if(trim($lines) < 2){
    echo "Server is NOT running. Starting server..." . PHP_EOL;
    $is_started = shell_exec( "php -f $exec_file > $log 2>&1 &" );
    if(empty($is_started)){
        echo "Server started." . PHP_EOL;
    }else{
        echo "Error. Server NOT started." . PHP_EOL;
    }
} else {
    echo "Server is running" . PHP_EOL;
}
