<?php

use Monolog\Logger;

require_once __DIR__."/../vendor/autoload.php";

class Log extends Logger{
	private $app;
	private $mail_transports = array();
	private $mailer;
	private $config;

	function __construct($app){

		$this->app = $app;
		parent:: __construct("file_upload_logger");

		$this->pushHandler($this->app['StreamHandler']);

		$mail_config = $this->app['mail_config']['routes'];
		$this->config = $this->app['mail_config']['config'];

		// Пробегаем по всем доступным конфигурациям отправки почты
		foreach ($mail_config as $server_name => $config) {

			// Создаем свой транспорт для кажного из серверов
			$this->mail_transports[$server_name] = Swift_SmtpTransport::newInstance($config['server'], $config['port'], $config['encription'])
		  		->setUsername($config['user'])
		  		->setPassword($config['password']);
		}
	}



	function sendMessage($theme, $msg){

		// Создаем сообщение для отправки
		$message = Swift_Message::newInstance($theme)
		  ->setFrom(array($this->config['from_mail'] => $this->config['from']))
		  ->setTo($this->config['admin_mail'])
		  ->setBody($msg);

		$i = 0;

		foreach ($this->mail_transports as $name => $transport) {
			$i++;

			try{

				// Пытаемся создать Swift_Mailer
				$this->mailer = Swift_Mailer::newInstance($transport);

				// Пытаемся отослать сообщение
				$result = $this->mailer->send($message);

				// Если сообщение успешно отосланно прекращаем попытки
				if($result){
					break;
				}

			}catch(Exception $e){
				$this->addWarning("Can't send message width $name smtp server \n $e");

				if(count($this->mail_transports) > $i ){
					continue;
				}else{
					$this->addError("Can't send message. No servers avilable!");
					break;
				}
			}
		}

	}

	function write($msg, $level){
		switch ($level){

			case 'INFO':
					$this->addInfo($msg);
					break;

			case 'NOTICE':
					$this->addNotice($msg);
					break;

			case 'WARNING':
					$this->addWarning($msg);
					break;

			case 'ERROR':
					$this->sendMessage($level, $msg);
					$this->addError($msg);
					break;

			case 'CRITICAL':
					$this->sendMessage($level, $msg);
					$this->addCritical($msg);
					break;

			case 'ALERT':
					$this->sendMessage($level, $msg);
					$this->addAlert($msg);
					break;

			case 'EMERGENCY':
					$this->sendMessage($level, $msg);
					$this->addEmergency($msg);
					break;

			default:
				$this->addDebug($msg);
		}

	}
}
