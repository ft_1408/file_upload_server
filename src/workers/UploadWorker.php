<?php

namespace Worker;
use \PDO;

// Воркер для загрузки файлов на сервер
class UploadWorker extends Worker{

	public function start(){
		// Получим конфигурацию очереди загрузки файлов
		$this->amqp_queue_config = $this->amqp_config['amqp']['queues'];

		// Инициализируем очередь AMQP и начинаем получение сообщений
		$this->initQueue();

	}

	// Проверка подлинности пользователя
	// если пользователь действителен возвращаеться 1
	// если нет - 0
	private function checkUserCredentials($meta){
		// Название таблицы
		$table = "users";

		// Формируем запрос
		$stmt = $this->db->prepare("SELECT COUNT(`email`) as `is_user`
							   FROM `$table`
							   WHERE `id` = ? AND
							   		 `email` = ?");

		// Забиндим параметры
		$stmt->execute(array($meta['user_id'], $meta['user_email']));
		// Получим ответ в форме асоциативного массива
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		// Вернем результат
		return $result['is_user'];
	}

	// Получение машинного имени файла
	private function getFileMachineName($fileName){
		$machineFileName = md5($fileName . time(). uniqid());

		return $machineFileName;
	}

	// Обрабатывает файл
	// Принимает массив - разсерелизованный файл из rabbitMq
	protected function processMessage($file){

		// Если пользователь прошел проверку на подленность
		if(($this->checkUserCredentials($file['meta']))){
			// Получаем подключение к коллекции для хранения файлов
			$mongoCollection = $this->mdb->flash->user->files;
			$meta = $file['meta'];

			// Получим имя файла с которым он храниться на сервере
			$machineName = $this->getFileMachineName($file['file_name']);

			// Получим имя текущего хоста
			$hostName = $this->app['main_config']['host_name'];


			// Формируем строку для сохранения
			$data = array();
			$data['user_file_dir'] = $meta['user_dir'];
			$data['user_email'] = $meta['user_email'];
			$data['user_id'] = $meta['user_id'];
			$data['user_fileName'] = $file['file_name'];
			$data['machine_fileName'] = $machineName;
			$data['host'] = $hostName;

			try{

				$data['file_size'] = $this->saveFile($machineName, $file['content']);

			}catch(Exception $e){
				throw new Exception("Ошибка при сохранения {$data['user_fileName']}
									файла от пользователя
									{$meta['user_email']}", 1);
			}

			// Вставляем строку
			$mongoCollection->insert($data);
		}else{

			// Если пользователь не проходит проверку - отправляем оповещение на почту
			throw new Exception("Попытка подмены данных пользователя
									{$meta['user_email']}", 1);
		}

	}

	// Сохранение файла на сервере
	private function saveFile($machineName, $file){
		// Получаем общий конфиг программы
		$config = $this->app['main_config'];

		// Получаем директорию хранения файлов
		$filesDir = $config['files_directory'];

		// Если директория не существует - создадим её
		if(!is_dir($filesDir)){
			mkdir($filesDir);
		}

		// Имя файла
		$fileSavePath = $filesDir . $machineName;

		// Сохраняем файл
		$result = file_put_contents($fileSavePath, $file);

		if($result){
			$result = filesize($fileSavePath);
		}
		return $result;
	}
}
