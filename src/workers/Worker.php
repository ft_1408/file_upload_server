<?php
namespace Worker;

// Абстрактный клас воркера
abstract class Worker{
    protected $app;
    protected $amqp_config;
    protected $channel;
    protected $mdb;
    protected $db;
    protected $amqp_queue_config;

    public function __construct($app){
        $this->app = $app;
        $this->amqp_config = $app['amqp_conf'];
        $this->mdb = $this->app['mdb'];
        $this->db = $app['dbh'];
    }

    // Получить сообщения из очереди
    public function getMsg($message){
        // "Распаковываем" пришедшие данные
        $msg = unserialize($message->body);

        try{

            // Обработка сообщения об удалении файла
            $this->processMessage($msg);

            // Удаляем сообщение из очереди
            $this->channel->basic_ack($message->delivery_info['delivery_tag']);

        }catch(Exception $e){
            $this->app['logger']->write($e->getMessage, "WARNING");
        }

    }

    // Метод - стартер воркера
    abstract protected function start();

    // Инициализация AMQP
    protected function initQueue(){
        // Создаем подключение и канал к ребиту
        $connection = $this->app['rabbit'];
        $this->channel = $connection->channel();

        // Декларируем обменник
        $this->channel->exchange_declare($this->amqp_queue_config['exchange'], $this->amqp_queue_config['exchange_type'], false, false, false);

        // Создаем очередь и биндинг
        $this->channel->queue_declare($this->amqp_queue_config['name'], false, false, false, false);
        $this->channel->queue_bind($this->amqp_queue_config['name'], $this->amqp_queue_config['exchange'], $this->amqp_queue_config['routing_key']);

        // Получаем тег консамера
        $consumer_tag = 'consumer_'. getmypid();
        // Создаем колбек на получение сообшений
        $this->channel->basic_consume($this->amqp_queue_config['name'], $consumer_tag, false, false, false, false, array($this, 'getMsg'));

        // Петля - если не было сообщений 10 секунд выходим
        while(count($this->channel->callbacks)) {
             $this->channel->wait(null, false, 10);
        }

        // Закрываем соединение
        $this->channel->close();
        $connection->close();
    }
}
