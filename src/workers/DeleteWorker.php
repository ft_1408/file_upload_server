<?php

namespace Worker;
use \Exception;

class DeleteWorker extends Worker{

	public function start(){
		// Получим конфигурацию очереди загрузки файлов
		$this->amqp_queue_config = $this->amqp_config['amqp']['delete_queue'];

		// !!ВАЖНО!! переопределяем название очереди в соответствии с названием хоста (может не работать)
		$this->amqp_queue_config['name'] = $this->app['main_config']['host_name'];
		$this->amqp_queue_config['routing_key'] = $this->app['main_config']['host_name'] . "_delete";

		// Инициализируем очередь AMQP и начинаем получение сообщений
		$this->initQueue();
	}


	// Обработка сообщения
	protected function processMessage($msg){
		if($this->checkCredentials($msg)){

			// Получаем общий конфиг программы
			$config = $this->app['main_config'];

			// Получаем директорию хранения файлов
			$filesDir = __DIR__ . '/../../' . $config['files_directory'];

			// Удалим файл с сервера
			unlink($filesDir ."/" . $msg['imachine_fileName']);
		}
	}

	private function checkCredentials($msg){

		// TODO: придумать проверку на полномочия пользователя для удаления данного файла

		return true;
	}

}
